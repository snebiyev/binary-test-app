from flask import Flask,jsonify,abort
from flask_sqlalchemy import SQLAlchemy
import mysql.connector
from dotenv import load_dotenv
import os

load_dotenv()

def create_app():
    app = Flask(__name__)
    db_name = os.getenv("DB_NAME")
    db_user = os.getenv("MYSQL_USER")
    db_password = os.getenv("MYSQL_PASSWORD")
    print(db_user)
    # Connect to MySQL database using SQLAlchemy
    app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+mysqlconnector://{db_user}:{db_password}@mysql-db:3306/{db_name}'
    db = SQLAlchemy(app)

    class User(db.Model):
        __tablename__ = 'users_binary'
        id = db.Column(db.Integer, primary_key=True)
        username = db.Column(db.String)
        surname = db.Column(db.String)
        email = db.Column(db.String)

        def serialize(self):
            return {
                'id': self.id,
                'surname': self.surname,
                'email': self.email
            }
        

    @app.route('/')
    def index():
        # Query the database for a first user
        user = User.query.first()

        if user is not None:
            # If a user was found, return it as JSON
            return jsonify(user.serialize())
        else:
            # If no user was found, return a 404 error
            abort(404)
            
        
    return app
