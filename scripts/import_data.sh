#!/bin/bash
printf -- "-------------------Reading .env file...-------------------\n"
source .env

printf -- "-------------------Importing data to MySQL container...-------------------\n"
# Import the data to the database
docker exec -i mysql-db mysql -h 127.0.0.1 --user=${MYSQL_USER} --password=${MYSQL_PASSWORD} ${DB_NAME}  < ${SQL_DUMP_FILE}


