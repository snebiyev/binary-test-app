-- create the `users` table
CREATE TABLE if not exists users_binary (
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(255) NOT NULL,
  surname VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

-- Clear the table
TRUNCATE TABLE users_binary;

-- insert some sample data
INSERT INTO
  users_binary (username, surname, email)
VALUES
  ('John', 'Doe', 'john.doe@example.com')