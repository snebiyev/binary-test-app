
## Running the Server

1.  Make sure you have given execute permission to `run.sh` and `clean.sh` files. You can do that by running the following commands in the terminal: 

`chmod +x run.sh clean.sh` 

2. Once you have given permission, you can start the app by running the following command:

`./run.sh` 

3.  Wait for the server to start. You will see a message in the terminal saying `* Running on http://0.0.0.0:5001/ (Press CTRL+C to quit)`.
    
4.  Once the server has started, open your web browser and go to `http://localhost:5001/`. You should see sample user information displayed on the web page.
    

![Screenshot from running app](screen.png)

5.  To stop the server and clean up any temporary files, run the following command in the terminal:

`./clean.sh` 

## Application Details

This app is built using Flask as the server and MySQL as the database. The server is containerized using Docker and can be easily deployed using the provided `Dockerfile` and `docker-compose.yml` files.

The following dependencies are used in this application:

-   Flask
-   SQLAlchemy
-   mysql-connector-python
-   dotenv

The `run.sh` and `clean.sh` scripts are used to start and stop the application respectively. They can be customized as per your requirements.
