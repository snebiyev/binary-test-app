# Description: Run the docker compose and import the data
printf -- "-------------------Changing permissions of scripts...-------------------\n"
chmod +x ./scripts/import_data.sh
chmod +x ./scripts/start_mysql_docker.sh
chmod +x ./scripts/start_app_docker.sh


printf -- "-------------------Starting MySQL and App containers...-------------------\n"
./scripts/start_mysql_docker.sh
printf -- "-------------------Waiting for MySQL to start... (10 seconds)-------------------\n"
sleep 10
./scripts/import_data.sh
./scripts/start_app_docker.sh

printf -- "-------------------Checking the logs...-------------------\n"
docker compose logs -f